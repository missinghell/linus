import os
import random

from flask import Flask, render_template, url_for

app = Flask(__name__)


class LinusRequest:
    def __init__(self):
        self._linus_count: int

        with open("linus.dat", "r") as linus:
            self._linus_count = int(linus.read().strip("\n"))

    def save_linus(self):
        with open("linus.dat", "w") as linus:
            linus.write(str(self._linus_count))

    @property
    def linus_count(self):
        return self._linus_count

    @linus_count.setter
    def linus_count(self, value):
        self._linus_count = value
        self.save_linus()


linus_reqs = LinusRequest()


@app.route("/")
def index():
    return render_template("index.html", x=linus_reqs.linus_count)


@app.route('/<int:number>')
def hello_world(number):  # put application's code here
    linus_reqs.linus_count += number

    linus_css_url = url_for("static", filename="css/main.css")

    message = f"<iframe src=\"static/bcs.mp3\" allow=\"autoplay\" style=\"display:none\" id=\"iframeAudio\"></iframe> \n<link rel=\"stylesheet\" href=\"{linus_css_url}\">\n"
    for _ in range(number):
        linus = random.choice([linus_img for linus_img in os.listdir("static/") if "linus" in linus_img])

        if random.randint(1,100) == 1:
            linus = "lol.webp"

        message += f"<img class=\"linus\" src=\"static/{linus}\" />"

    return message


@app.route("/marquee/<int:number>")
def marquee(number):
    linus_reqs.linus_count += number

    linus_css_url = url_for("static", filename="css/main.css")

    message = f"<link rel=\"stylesheet\" href=\"{linus_css_url}\">\n"
    for i in range(number):
        linus = random.choice([linus_img for linus_img in os.listdir("static/") if "linus" in linus_img])
        message += f"<marquee scrollamount=\"{i}\"><img class=\"linus\" src=\"/static/{linus}\" /></marquee>"

    return message


@app.route("/tattam/<int:number>")
def tattam(number):
    linus_reqs.linus_count += number

    linus_css_url = url_for("static", filename="css/main.css")

    message = f"<link rel=\"stylesheet\" href=\"{linus_css_url}\">\n"
    for i in range(number):
        linus = random.choice([linus_img for linus_img in os.listdir("static/") if "tattam" in linus_img])
        message += f"<img class=\"linus\" src=\"/static/{linus}\" />"

    return message


@app.route("/rachid/")
def rachid():
    rachid_url = url_for("static", filename="css/rachid.css")

    message = f"<link rel=\"stylesheet\" href=\"{rachid_url}\">\n"
    message += "<h1>YOU DO NOT HAVE THE POWER TO SUMMON RACHID!!!!!!</h1>"

    return message

@app.route("/vim/")
def vim():
    return render_template("vim.html")

if __name__ == '__main__':
    app.run()
